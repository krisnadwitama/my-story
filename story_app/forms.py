from django import forms

class Message_Form(forms.Form):
    message = forms.CharField(label='Status', max_length=300, widget=forms.TextInput(attrs={'size':50}))