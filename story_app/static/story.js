$(document).ready(function() {
 	$("#dark_button").click(function(){
 		localStorage.setItem("css", "/static/dark_theme.css");
 		themeSet();
 	});
 	$("#light_button").click(function(){
 		localStorage.setItem("css", "/static/light_theme.css");
 		themeSet();
 	});
 	$("#dark_button").mouseover(function(){
 		$("#css").attr("href", "/static/dark_theme.css");
 	});
 	$("#dark_button").mouseout(function(){
 		themeSet();
 	});
 	$("#light_button").mouseover(function(){
 		$("#css").attr("href", "/static/light_theme.css");
 	});
 	$("#light_button").mouseout(function(){
 		themeSet();
 	});
 	themeSet();
 	$( function() {
		$( "#accordion" ).accordion();
	} );
	$("#registration_form").submit(function(event){
		var csrftoken = $("[name='csrfmiddlewaretoken']").attr("value");
		event.preventDefault();
		var data = {
			"id_field" : $('#id_id_field').val(),
			"email_field" : $('#id_email_field').val(),
			"password_field" : $('#id_password_field').val(),
			"confirm_field" : $('#id_confirm_field').val()
		};
		$.ajax({
			method: "POST",
			url: "/registration/validation",
			data: {
				"data" : JSON.stringify(data)
			},
			headers: {
				"X-CSRFToken": csrftoken
			},
			success: function(data, status) {
			}
		});
	});
});

function themeSet(){
	if(typeof(Storage) !== "undefined"){
		var theme = localStorage.getItem("css");
		if(theme === null){
			$("#css").attr("href", "/static/light_theme.css");
		}
		else{
			$("#css").attr("href", theme);
		}
	}
	else {
		$("#css").attr("href", "/static/light_theme.css");
	}
}