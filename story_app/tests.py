import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *

class StoryAppUnitTest(TestCase):
	def test_story_app_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_story_app_using_index_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')

	def test_story_app_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_landing_page_content(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Hello, apa kabar?', html_response)

	def test_create_message_models(self):
		cnt_prev = Message.objects.all().count()
		Message.objects.create(message='Test')
		cnt = Message.objects.all().count()
		self.assertEqual(cnt, cnt_prev+1)

	def test_data_in_form_is_displayed(self):
		message = "Hai"
		response = Client().post('/', {'message': message})
		self.assertIn(message, response.content.decode())

	def test_about_me_url_is_exist(self):
		response = Client().get('/about_me')
		self.assertEqual(response.status_code, 200)

	def test_about_me_func(self):
		found = resolve('/about_me')
		self.assertEqual(found.func, about_me)

	def test_page_using_about_me_template(self):
		response = Client().get('/about_me')
		self.assertTemplateUsed(response, 'about_me.html')

	def test_about_me_page_content(self):
		request = HttpRequest()
		response = about_me(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Nama', html_response)
		self.assertIn('NPM', html_response)
		self.assertIn('I Made Krisna Dwitama', html_response)
		self.assertIn('1806133881', html_response)
		self.assertIn('<img', html_response)

class StoryAppFunctionalTest(unittest.TestCase):
	# Kode di bawah ini adalah test untuk story sampai dengan
	# pembatas yang tertulis di bawah
	def setUp(self):
		chrome_options = webdriver.ChromeOptions()
		chrome_options.add_argument('headless')
		self.browser = webdriver.Chrome(chrome_options=chrome_options)

	def tearDown(self):
		self.browser.quit()

	def test_message_form_submit_button(self):
		self.browser.get('https://tama-story.herokuapp.com/')
		time.sleep(5)
		message = 'Functional Test Submit Button'
		input_box = self.browser.find_element_by_name('message')
		input_box.send_keys(message)
		time.sleep(5)
		input_box.submit()
		time.sleep(5)
		self.assertIn(message, self.browser.page_source)

	def test_message_form_enter_key(self):
		self.browser.get('https://tama-story.herokuapp.com/')
		time.sleep(5)
		message = 'Functional Test Enter Key'
		input_box = self.browser.find_element_by_name('message')
		input_box.send_keys(message)
		time.sleep(5)
		input_box.send_keys(Keys.ENTER)
		time.sleep(5)
		self.assertIn(message, self.browser.page_source)

	def test_redirect_link_from_home(self):
		self.browser.get('https://tama-story.herokuapp.com/')
		time.sleep(5)
		action = webdriver.common.action_chains.ActionChains(self.browser)
		hyperlink = self.browser.find_element_by_xpath('//*[@id="button_link"]')
		action.move_to_element(hyperlink).perform()
		time.sleep(5)
		hyperlink.click()
		time.sleep(5)
		self.assertEqual("About Me", self.browser.title)

	def test_redirect_link_to_home(self):
		self.browser.get('https://tama-story.herokuapp.com/about_me')
		time.sleep(5)
		action = webdriver.common.action_chains.ActionChains(self.browser)
		hyperlink = self.browser.find_element_by_xpath('//*[@id="button_link"]')
		action.move_to_element(hyperlink).perform()
		time.sleep(5)
		hyperlink.click()
		time.sleep(5)
		self.assertEqual("Status Message Form", self.browser.title)

	# Kode di bawah ini adalah test untuk challenge
	def test_background_color(self):
		self.browser.get('https://tama-story.herokuapp.com/')
		time.sleep(5)
		body = self.browser.find_element_by_xpath('/html/body')
		color = body.value_of_css_property("background-color")
		self.assertEqual("rgba(217, 255, 255, 1)", color)

	def test_image_width(self):
		self.browser.get('https://tama-story.herokuapp.com/about_me')
		time.sleep(5)
		img = self.browser.find_element_by_xpath('/html/body/div/div/img')
		width = img.value_of_css_property("width")
		self.assertEqual("200px", width)

	def test_random_link_that_not_exist(self):
		self.browser.get('https://tama-story.herokuapp.com/asdfghjkl')
		time.sleep(5)
		self.assertIn("Page not found", self.browser.title)

	def test_submit_empty_string(self):
		self.browser.get('https://tama-story.herokuapp.com/')
		time.sleep(5)
		input_box = self.browser.find_element_by_name('message')
		time.sleep(5)
		input_box.submit()
		time.sleep(5)
		self.assertIn("errorlist", self.browser.page_source)
		self.assertIn("This field is required.", self.browser.page_source)
