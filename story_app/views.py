from django.shortcuts import render
from django.http import HttpResponseRedirect

from .forms import Message_Form

from .models import Message

def index(request):
	form = Message_Form(request.POST or None)
	response = {}
	if (request.method == 'POST' and form.is_valid()):
		response['message'] = form.cleaned_data['message']
		message = Message(message=response['message'])
		message.save()

	message_list = Message.objects.all().values()
	response = {'message_form': form, 'message_list': message_list}
	return render(request, 'index.html', response)

def about_me(request):
	return render(request, 'about_me.html')