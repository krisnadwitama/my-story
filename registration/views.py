from django.shortcuts import render
from django.http import HttpResponseRedirect

from .forms import Registration_Form

from .models import Registration

def registration(request):
	form = Registration_Form(request.POST or None)
	response = {'registration_form': form}
	return render(request, 'registration.html', response)

def validation(request):
	form = Registration_Form(request.POST or None)
	response = {}
	if (request.method == 'POST' and form.is_valid()):
		if(data['password_field'] == data['confirm_field']):
			user = Registration(id_input=data['id_field'], email_input=data['email_field'], password_input=data['password_field'])
			user.save()
	response = {'registration_form': form}
	return render(request, 'registration.html', response)