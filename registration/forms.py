from django import forms

class Registration_Form(forms.Form):
    id_field = forms.CharField(label='Id', max_length=300, widget=forms.TextInput(attrs={'size':50}))
    email_field = forms.CharField(label='Email', max_length=300, widget=forms.EmailInput(attrs={'size':50}))
    password_field = forms.CharField(label='Password', max_length=300, widget=forms.PasswordInput(attrs={'size':50}))
    confirm_field = forms.CharField(label='Confirm password', max_length=300, widget=forms.PasswordInput(attrs={'size':50}))